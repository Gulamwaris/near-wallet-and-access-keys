# Near Wallet And Access Keys

This is a read me file for a demo flutter project where Near Network is used to create a wallet, logging into the existing wallet and managing the public, private keys of the wallet.

## Prerequisite

1. Basic knowledge of building apps on Flutter.
2. VSCode, Flutter & dart installed on your system.
3. [near-rest-server](https://gitlab.com/Gulamwaris/near-rest-server.git) cloned on your system.

## Installation

```bash
git clone https://gitlab.com/Gulamwaris/near-wallet-and-access-keys.git
```

1. If you have all the prerequisites taken care of then you are good to go, just use the command given above to clone this repo. Open your terminal at the folder's location where you want to clone the repo and paste the above command in your terminal and hit enter.

2. After cloning the repo open the project in VSCode, if you see errors in the project, that is because none of the dependencies used in the project are installed hence you will have to go to the pubspec.yaml file in the project and save the file once to automatically install dependencies or go to the integrated terminal and paste the command given below.

```bash
flutter pub get
```

3. Make sure [near-rest-server](https://gitlab.com/Gulamwaris/near-rest-server.git) has connected successfully to your localhost and running in background.

4. Run the app in your simulator using VSCode.

## Files

1. lib -> main.dart file is the root of the application.
2. lib -> signup_page.dart file has the code regarding the Signup page of the application, where the user can enter his/her desired account name to create wallet.
3. lib -> login_page.dart file has the code regarding the Login page of the application, where the user can enter his/her seedPhrase/passPhrase to login into existing wallet.
4. lib -> logout_page.dart file has the code regarding the Logout page of the application, where the user has button to logout of the wallet and copy seedPhrase/passPhrase.



## License
[MIT](https://choosealicense.com/licenses/mit/)
