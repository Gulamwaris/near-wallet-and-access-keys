import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'signup_page.dart';

//This is the logout page. This page is showed after the user has successfully created a account and logged in.
class LogoutPage extends StatefulWidget {
  //Fetching the passPhrase from login screen to display it on the logout screen.
  final String passPhrase;

//Constructor for LogoutPage class.
  const LogoutPage({
    Key? key,
    required this.passPhrase,
  }) : super(key: key);

  @override
  State<LogoutPage> createState() => _LogoutPageState();
}

class _LogoutPageState extends State<LogoutPage> {
  //Clear data function for clearing the data saved in shared preferences if user logs out.
  clearData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.clear();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Logout"),
      ),
      // Center is a layout widget. It takes a single child and positions it in the middle of the parent.
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            //Text widget to display the users passphrase on the screen.
            Container(
              margin: const EdgeInsets.only(left: 20, right: 20, bottom: 30),
              width: MediaQuery.of(context).size.width,
              child: Row(
                children: [
                  Expanded(
                    child: Text(
                      "Passphrase: ${widget.passPhrase}",
                      maxLines: 4,
                      style: const TextStyle(fontSize: 20),
                    ),
                  ),
                  //Icon button to copy passphrase for the user to save it.
                  IconButton(
                      onPressed: () {
                        //if user copies the passphrase by clicking on the copy button this message will be shown.
                        ScaffoldMessenger.of(context)
                            .showSnackBar(const SnackBar(
                                content: Text(
                          "PassPhrase copied!",
                          style: TextStyle(
                              fontWeight: FontWeight.w600, fontSize: 20),
                        )));
                        //Copying the passphrase to user's device clipboard.
                        Clipboard.setData(
                            ClipboardData(text: widget.passPhrase));
                      },
                      icon: const Icon(Icons.copy)
                  ),
                ],
              ),
            ),
            //Description text above the button.
            const Text(
              'Press this button to Logout:',
              style: TextStyle(fontSize: 20),
            ),
            //Created a button widget for loggin out the user and navigating back to the login page.
            Container(
              margin: const EdgeInsets.only(left: 20, right: 20, top: 10),
              //Width of the button.
              width: MediaQuery.of(context).size.width,
              //Height of the button.
              height: 50,
              child: ElevatedButton(
                onPressed: () async {
                  //Clear data function called for clearing the data saved in shared preferences if user logs out.
                  await clearData();
                  //Navigator called for navigating back to the login screen.
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const SignUpPage()),
                      (route) => false);
                },
                //This is the text shown inside the button widget.
                child: const Text(
                  "Logout",
                  style: TextStyle(fontSize: 20),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
