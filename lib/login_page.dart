import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'logout_page.dart';

//This is the login page where the user has to enter passphrase to log into account. If the user's account is found successfully then the user is logged in and navigated to logout page.
class LoginPage extends StatefulWidget {
  //Constructor for LoginPage class.
  const LoginPage({
    Key? key,
  }) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  //Textediting controller used for fetching the text inserted inside the textfield.
  TextEditingController textEditingController = TextEditingController();
  //State varibale of type bool used to toggle circular progess indicator on the screen
  bool isLoading = false;
  //State variables of type string to store the account details such as account name, keys and passPhrase in the variables.
  String publicKey = "", privateKey = "", accountName = "";

  //API function for logging the user in Near wallet. It returns a boolean value to check if the API call was successfull or not.
  Future<bool> loginUser(String passPhrase) async {
    try {
      //Url of the server that is hosted on your localhost.
      Uri uri = Uri.parse("http://localhost:3000/user_details");
      //POST request with URL, request body and required headers for the API call.
      Response response = await http.post(
        uri,
        body: jsonEncode({"seed_phrase": passPhrase}),
        headers: {
          "Content-type": "application/json",
          "Accept": "application/json"
        },
      );
      //If the response status code of the API call is 200 then it means that the API call was successfull and the user's account has been created.

      if (response.statusCode == 200) {
        //Decoding the json response that came back from the server.
        var body = jsonDecode(response.body);
        //Setting the state variables for publickey, privatekey and accountname.
        setState(() {
          publicKey = body["publicKey"];
          privateKey = body["privateKey"];
          accountName = body["accountName"];
        });
        //Returning true if the API call is successfull.
        return true;
      }
      //Returning false if the response status code is anything other than 200. Here more validations can be done for every status code for example 500, 404, 400 etc.
      return false;
    } catch (err) {
      print("Error : $err");
      //Returning false if the Try block fails.
      return false;
    }
  }

//Function for saving the data in device local storage using sharedpreferances.
  saveData() async {
    final preferences = await SharedPreferences.getInstance();
    //Saving the privateKey, publicKey, accountName in local storage in the form of key, value pair.
    preferences.setString("privateKey", privateKey);
    preferences.setString("publicKey", publicKey);
    preferences.setString("accountName", accountName);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Login'),
      ),
      // Center is a layout widget. It takes a single child and positions it in the middle of the parent.
      body: Center(
        child: Container(
          margin: const EdgeInsets.only(left: 20, right: 20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              //Description text shown above the textfield for the user to understand what to enter in textfield.
              const Text(
                'Passphrase:',
                style: TextStyle(fontSize: 20),
              ),
              //Created a textfield widget for user to enter passphrase for loggin in if the user already has a account.
              Container(
                margin: const EdgeInsets.only(top: 10, bottom: 30),
                child: TextField(
                  autocorrect: false,
                  minLines: 1,
                  maxLines: 3,
                  controller: textEditingController,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.teal)),
                  ),
                  style: const TextStyle(fontSize: 20),
                ),
              ),
              //Created a button widget for calling the API to login user and navigate to next page if the account is found successfully.
              isLoading
                  //If the isLoading state variable is true then display a circular progess indicator instead of button.
                  ? Container(
                      child: const CircularProgressIndicator(),
                      alignment: Alignment.center,
                    )
                  : SizedBox(
                      //Width of the button.
                      width: MediaQuery.of(context).size.width,
                      //Height of the button.
                      height: 50,
                      child: ElevatedButton(
                        onPressed: () async {
                          if (textEditingController.text.isEmpty) {
                            //if the textfield is empty then this Snackbar error message will be shown.
                            ScaffoldMessenger.of(context)
                                .showSnackBar(const SnackBar(
                                    content: Text(
                              "Please enter PassPhrase!",
                              style: TextStyle(
                                  fontWeight: FontWeight.w600, fontSize: 20),
                            )));
                          } else {
                            //Setting the isLoading state variable to true to show a circular progress indicator instead of button so the user doesnt click it twice.
                            setState(() {
                              isLoading = true;
                            });
                            //API function called for logging the user in Near wallet. It returns a boolean value to check if the API call was successfull or not.
                            bool? success =
                                await loginUser(textEditingController.text);
                            //If the account is found successfully then save the keys in the local storage and navigate to Logout page.
                            if (success == true) {
                              //Function called for saving the data in device local storage using sharedpreferances.
                              saveData();
                              setState(() {
                                isLoading = false;
                              });
                              //Navigator used to navigate to next page which is Logout page.
                              Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => LogoutPage(
                                          passPhrase:
                                              textEditingController.text)));
                            }
                          }
                        },
                        //This is the text shown inside the button widget.
                        child: const Text(
                          "Login",
                          style: TextStyle(fontSize: 20),
                        ),
                      ),
                    ),
            ],
          ),
        ),
      ),
    );
  }
}
