import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'login_page.dart';
import 'logout_page.dart';

//This is the Signup page where the user has to enter account id/ wallet id to create a account. If the user's account is created successfully then the user is logged in and navigated to logout page.
class SignUpPage extends StatefulWidget {
  //Constructor for SignUpPage class.
  const SignUpPage({
    Key? key,
  }) : super(key: key);

  @override
  State<SignUpPage> createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  //Textediting controller used for fetching the text inserted inside the textfield.
  TextEditingController textEditingController = TextEditingController();
  //State varibale of type bool used to toggle circular progess indicator on the screen
  bool isLoading = false;
  //State variables of type string to store the account details such as account name, keys and passPhrase in the variables.
  String passPhrase = "", publicKey = "", privateKey = "", accountName = "";

  //API function for creating a new account/wallet on Near. It returns a boolean value to check if the API call was successfull or not.
  Future<bool> createUser(String accountID) async {
    try {
      //Url of the server that is hosted on your localhost.
      Uri uri = Uri.parse("http://localhost:3000/create_user");
      //POST request with URL, request body and required headers for the API call.
      Response response = await http.post(
        uri,
        body: jsonEncode({"name": accountID}),
        headers: {
          "Content-type": "application/json",
          "Accept": "application/json"
        },
      );
      //If the response status code of the API call is 200 then it means that the API call was successfull and the user's account has been created.

      if (response.statusCode == 200) {
        //Decoding the json response that came back from the server.
        var body = jsonDecode(response.body);
        //The json response for the publicKey has another 2 fields called accountId and allKeys in it so decoding it further to extract only the publicKey from the allKeys field.

        setState(() {
          passPhrase = body["passPhrase"];
          publicKey = body["publicKey"];
          privateKey = body["privateKey"];
          accountName = body["accountName"];
        });
        //Returning true if the API call is successfull.
        return true;
      }
      //Returning false if the response status code is anything other than 200. Here more validations can be done for every status code for example 500, 404, 400 etc.
      return false;
    } catch (err) {
      print("Error : $err");
      //Returning false if the Try block fails.
      return false;
    }
  }

//Function for saving the data in device local storage using sharedpreferances.
  saveData() async {
    final preferences = await SharedPreferences.getInstance();
    //Saving the privateKey, publicKey, accountName in local storage in the form of key, value pair.
    preferences.setString("privateKey", privateKey);
    preferences.setString("publicKey", publicKey);
    preferences.setString("accountName", accountName);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Sign Up'),
      ),
      // Center is a layout widget. It takes a single child and positions it in the middle of the parent.
      body: Container(
        margin: const EdgeInsets.only(left: 20, right: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            //Description text shown above the textfield for the user to understand what to enter in textfield.
            const Text(
              'AccountName:',
              style: TextStyle(fontSize: 20),
            ),
            //Created a textfield widget for user to enter account id/wallet id for creating a account on Near platform.
            Container(
              margin: const EdgeInsets.only(top: 10, bottom: 30),
              child: TextField(
                autocorrect: false,
                controller: textEditingController,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.teal)),
                ),
                style: const TextStyle(fontSize: 20),
              ),
            ),
            //Created a button widget for calling the API to create account and navigate to next page if the account creation is successfull.
            isLoading
                //If the isLoading state variable is true then display a circular progess indicator instead of button.
                ? Container(
                    alignment: Alignment.center,
                    child: const CircularProgressIndicator(),
                    margin: const EdgeInsets.only(bottom: 20),
                  )
                : Container(
                    margin: const EdgeInsets.only(bottom: 20),
                    //Width of the button.
                    width: MediaQuery.of(context).size.width,
                    //Height of the button.
                    height: 50,
                    child: ElevatedButton(
                      onPressed: () async {
                        if (textEditingController.text.isEmpty) {
                          //if the textfield is empty then this Snackbar error message will be shown.
                          ScaffoldMessenger.of(context)
                              .showSnackBar(const SnackBar(
                                  content: Text(
                            "Please enter an AccountName!",
                            style: TextStyle(
                                fontWeight: FontWeight.w600, fontSize: 20),
                          )));
                        } else {
                          //Setting the isLoading state variable to true to show a circular progress indicator instead of button so the user doesnt click it twice.
                          setState(() {
                            isLoading = true;
                          });
                          bool? success =
                              await createUser(textEditingController.text);
                          //If the account creation was successfull then save the keys in the local storage and navigate to Logout page.
                          if (success == true) {
                            //Function called for saving the data in device local storage using sharedpreferances.
                            saveData();
                            setState(() {
                              isLoading = false;
                            });
                            //Navigator used to navigate to next page which is Logout page.
                            Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        LogoutPage(passPhrase: passPhrase)));
                          }
                        }
                      },
                      //This is the text shown inside the button widget.
                      child: const Text(
                        "Create Account",
                        style: TextStyle(fontSize: 20),
                      ),
                    ),
                  ),
            //Text button used to navigate to Login page if the user already has an account and its passphrase to login.
            Container(
              alignment: Alignment.center,
              //InkWell is a widget used to make any type of widget clickable.
              child: InkWell(
                onTap: () {
                  //Navigator to navigate to login screen after the text is tapped by user.
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const LoginPage()));
                },
                //Text that the user will tap to navigate.
                child: const Text(
                  "Login with PassPhrase?",
                  style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.w600,
                      color: Colors.blueAccent),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
